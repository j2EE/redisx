# RedisX

>>什么是redisx？

>1、包含几乎所有redis线程池实现以及用法；

>2、包含redis Master Slave 主备实现；

>3、包含redis 读写分离；

>4、包含redis Sentinel 线程池；

>5、包含redis Sentinel Salve 线程池；

>5、包含redis Sentinel Sharded Master Slave 线程池；

>6、包含redis 对象序列化压缩算法，gz和lzma等；

具体用法：

```
public class TestRedisUtils
{
    public static String value="2017年春运来得早，又赶上火车票预售期由60天调整至30天，购票期相对集中。对准备回家过年的人们而言，回家的火车票还好买吗？";

    public static void main(String[] args) throws InterruptedException
    {
        
        for (int i = 0; i < 500; i++)
        {
            new ThreadTest("treahd"+i).start();

        }      
}

class ThreadTest extends Thread
{
    private String name;

    public ThreadTest()
    {
        super();
    }

    public ThreadTest(String name)
    {
        super();
        this.name = name;
    }

    @Override
    public void run()
    {
        for (int i = 0; i < 10; i++)
        {
            String string = RedisSharedMasterSlaveSentinelUtil.setObject(name+i,TestRedisUtils.value+name+i);             
                    System.out.println(string + "=" + i);

        }

        for (int i = 0; i < 10; i++)
        {
            String value = RedisSharedMasterSlaveSentinelUtil.getObject(name + i, String.class);
            if(null != value)
            {
                System.out.println(value);
            }
            long l = RedisSharedMasterSlaveSentinelUtil.del(name + i);
            System.out.println(l + "=" + i);

        }
    }
}


```

## 推荐开源项目[zbus](https://www.oschina.net/p/zbus),[talent-aio](https://www.oschina.net/p/talent-aio),[nredis-proxy](https://www.oschina.net/p/nredis-proxy)